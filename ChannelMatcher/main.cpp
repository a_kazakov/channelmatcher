#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <initializer_list>
#include <limits>

#include "constants.h"
#include "ArgParser.h"

using std::string;
using std::stringstream;
using std::cout;
using std::cerr;
using std::endl;
using std::numeric_limits;

#include "Image.h"

#include "main.hpp"

void print_help(const char *argv0)
{
	const char *usage =
		R"(where PARAMS are from list:

--align [--gray-world | --unsharp | --autocontrast [<fraction>]]
    align images with one of postprocessing functions

--gaussian <sigma> [<radius>=1]
    gaussian blur of image, 0.1 < sigma < 100, radius = 1, 2, ...

--gaussian-separable <sigma> [<radius>=1]
    same, but gaussian is separable

--sobel-x
    Sobel x derivative of image

--sobel-y
    Sobel y derivative of image

--unsharp
    sharpen image

--gray-world
    gray world color balancing

--autocontrast [<fraction>=0.0]
    autocontrast image. <fraction> of pixels must be croped for robustness

--resize <scale>
    resize image with factor scale. scale is real number > 0

--canny <threshold1> <threshold2>
    apply Canny filter to grayscale image. threshold1 < threshold2,
    both are in 0..360

--custom <kernel_string>
    convolve image with custom kernel, which is given by kernel_string, example:
    kernel_string = '1,2,3;4,5,6;7,8,9' defines kernel of size 3

[<param>=default_val] means that parameter is optional.
)";
	cout << "Usage: " << argv0 << " <input_image_path> <output_image_path> "
		<< "PARAMS" << endl;
	cout << usage;
}

#define PROCESS_ALGO_0(pname, fname) \
{\
	int size = ap.getSize(pname); \
	if (size != -1) { \
		if (processed) { \
			throw std::string("Multiple processing agorithms are not allowed."); \
		} \
		processed = true; \
		dst_image = Main::fname (src_image); \
	} \
}

#define PROCESS_ALGO_1(pname, t1, fname, req) \
{\
	int size = ap.getSize(pname); \
	if (size != -1) { \
		if (processed) { \
			throw std::string("Multiple processing agorithms are not allowed."); \
		} \
		processed = true; \
		if (size < req) { \
			throw std::string("Not enougn arguments for " pname "."); \
		} \
		switch (size) { \
			case 0: dst_image = Main::fname (src_image); break; \
			case 1: dst_image = Main::fname (src_image, read_value<t1>(ap.getParam(0, pname))); break;  \
			default: break; \
		} \
	} \
}

#define PROCESS_ALGO_2(pname, t1, t2, fname, req) \
{\
	int size = ap.getSize(pname); \
	if (size != -1) { \
		if (processed) { \
			throw std::string("Multiple processing agorithms are not allowed."); \
		} \
		processed = true; \
		if (size < req) { \
			throw std::string("Not enougn arguments for " pname "."); \
		} \
		switch (size) { \
			case 0: dst_image = Main::fname (src_image); break; \
			case 1: dst_image = Main::fname (src_image, read_value<t1>(ap.getParam(0, pname))); break;  \
			case 2: dst_image = Main::fname (src_image, read_value<t1>(ap.getParam(0, pname)),  \
			                                               read_value<t2>(ap.getParam(1, pname))); break;  \
			default: break; \
		} \
	} \
}

int main(int argc, char **argv)
{
	try {
		ArgParser ap(argc, argv);

		if (ap.getSize("--help") != -1) {
			print_help(argv[0]);
			return 0;
		}

		if (ap.getSize() < 2) {
			throw string("You should provide filenames");
		}

		Image<Px_8u3c> src_image(argv[1]), dst_image;

		bool processed = false;
		bool matched = false;

		if (ap.getSize("--align") != -1) {
			int sp = 1;
			if (ap.getSize("--subpixel") != -1) {
				sp = 2;
				if (ap.getSize("--subpixel") != 0) {
					sp = read_value<int>(ap.getParam(0, "--subpixel"));
				}
			}
			if (ap.getSize("--gray-world") != -1) {
				dst_image = Main::align(src_image, "--gray-world", sp);
			} else if (ap.getSize("--autocontrast") != -1) {
				double f = 0.0;
				if (ap.getSize("--autocontrast") != 0) {
					f = read_value<double>(ap.getParam(0, "--autocontrast"));
				}
				dst_image = Main::align(src_image, "--autocontrast", sp, f);
			} else if (ap.getSize("--unsharp") != -1) {
				dst_image = Main::align(src_image, "--unsharp", sp);
			} else {
				dst_image = Main::align(src_image, "", sp);
			}
			processed = true;
			matched = true;
		}
		PROCESS_ALGO_0("--sobel-x", sobel_x)
		PROCESS_ALGO_0("--sobel-y", sobel_y)
		if (!matched) {
			PROCESS_ALGO_0("--unsharp", unsharp)
			PROCESS_ALGO_0("--gray-world", gray_world)
			PROCESS_ALGO_1("--autocontrast", double, autocontrast, 0)
		}
		PROCESS_ALGO_1("--resize", double, resize, 1)
		PROCESS_ALGO_1("--custom", std::string, custom, 1)
		PROCESS_ALGO_2("--gaussian", double, int, gaussian, 1)
		PROCESS_ALGO_2("--gaussian-separable", double, int, gaussian_separable, 1)
		PROCESS_ALGO_1("--median", int, median, 1)
		PROCESS_ALGO_1("--median-linear", int, median_linear, 1)
		PROCESS_ALGO_1("--median-const", int, median_const, 1)
		PROCESS_ALGO_2("--canny", int, int, canny, 2)
		if (!processed) {
			throw string("I don't know, what to do");
		}
		dst_image.save(argv[2]);
	}
	catch (const std::exception &ex) {
		cerr << "Internal error: " << ex.what() << endl;
		return 1;
	}
	catch (const string &s) {
		cerr << "Error: " << s << endl;
		cerr << "For help type: " << endl << argv[0] << " --help" << endl;
		return 1;
	}
}
