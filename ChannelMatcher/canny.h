#pragma once

#define _USE_MATH_DEFINES
#include <cmath>
#include <functional>
#include <queue>

#include "Image.h"

#include "constants.h"

#include "gauss.h"
#include "sobel.h"

#include "channels_operations.h"

Image<Px_8u> canny(const Image<Px_16s> &src, int l1 = CANNY_L1, int l2 = CANNY_L2);
