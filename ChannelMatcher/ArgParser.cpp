#include "ArgParser.h"

ArgParser::ArgParser(int argc, char *argv[]) : argc(argc), argv(argv)
{
	std::pair<int, int> cur_pair = std::make_pair(0, 0);
	std::string cur_name = "";
	for (int i = 0; i < argc; ++i) {
		std::string tmp(argv[i]);
		if (tmp.length() >= 2 && tmp.substr(0, 2) == "--") {
			cur_pair.second = i;
			stops[cur_name] = cur_pair;
			cur_pair.first = i + 1;
			cur_name = tmp;
		}
	}
	cur_pair.second = argc;
	stops[cur_name] = cur_pair;
}

int ArgParser::getSize(const std::string &name)
{
	if (!stops.count(name)) {
		return -1;
	} else {
		auto tmp = stops[name];
		return tmp.second - tmp.first;
	}
}

char *ArgParser::getParam(int n, const std::string &name)
{
	return argv[stops[name].first + n];
}
