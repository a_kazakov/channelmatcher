#include "sharpen.h"

void sharpen_channel(Image<Px_8u> &img)
{
	Image<int> conv(3, 3, 0);
	const int raw_conv[9] ={ -1, -4, -1, -4, 26, -4, -1, -4, -1 };
	std::memcpy(conv.ptr(0, 0), raw_conv, 9 * sizeof(int));
	Image<Px_16s> res = applyConvultion(to16bit(img), conv, 6);
	img = to8bit(res, 0, 255);
}

void sharpen(Image<Px_8u3c> &img)
{
	Image<Px_8u> c1, c2, c3;
	splitChannels(c1, c2, c3, img);
	c1.checkBorders(2);
	c2.checkBorders(2);
	c3.checkBorders(2);
	c1.fillBorders();
	c2.fillBorders();
	c3.fillBorders();
	sharpen_channel(c1);
	sharpen_channel(c2);
	sharpen_channel(c3);
	img = mixChannels(c1, c2, c3);
}
