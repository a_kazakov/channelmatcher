#include "convultions.h"

Image<Px_16s> applyConvultion_mt(const Image<Px_16s> &src, const Image<int> &conv, int divisor)
{
	Image<Px_16s> dst = createSimilarImage<Px_16s>(src);
	int rv = conv.getHeight() / 2;
	int rh = conv.getWidth() / 2;
	std::vector<std::thread> active_threads;
	int threads = std::thread::hardware_concurrency();
	int start_offset = rv * src.getStride() + rh;
	int next_row_offset = src.getStride() - conv.getWidth();
	auto start_pconv = conv.ptr(0, 0);
	for (int t = 0; t < threads; ++t) {
		active_threads.push_back(std::thread([=, &src, &dst, &conv](int f, int t, int step) {
			for (int i = f; i < t; i += step) {
				auto *pdst = dst.ptr(i, 0);
				auto *psrc = src.ptr(i, 0);
				for (int j = 0; j < src.getWidth(); ++j) {
					int s = 0;
					auto *lpsrc = psrc - start_offset;
					auto *pconv = start_pconv;
					for (int ii = 0; ii < conv.getHeight(); ++ii) {
						for (int jj = 0; jj < conv.getWidth(); ++jj) {
							s += static_cast<int>(*lpsrc) * *pconv;
							++lpsrc, ++pconv;
						}
						lpsrc += next_row_offset;
					}
					*pdst = s / divisor;
					++pdst;
					++psrc;
				}
			}
		}, t, src.getHeight(), threads));
	}
	std::for_each(active_threads.begin(), active_threads.end(), [](std::thread &t) {
		t.join();
	});
	return dst;
}

Image<Px_16s> applyConvultion(const Image<Px_16s> &src, const Image<int> &conv, int divisor)
{
#ifndef NO_MT
	if (std::thread::hardware_concurrency() > 1) {
		try {
			return applyConvultion_mt(src, conv, divisor);
		}
		catch (std::exception &ex) {
			std::cerr << "MT error: " << ex.what() << std::endl;
		}
	}
#endif
	Image<Px_16s> dst = createSimilarImage<Px_16s>(src);
	int rv = conv.getHeight() / 2;
	int rh = conv.getWidth() / 2;
	int start_offset = rv * src.getStride() + rh;
	int next_row_offset = src.getStride() - conv.getWidth();
	auto start_pconv = conv.ptr(0, 0);
	for (int i = 0; i < src.getHeight(); ++i) {
		auto *pdst = dst.ptr(i, 0);
		auto *psrc = src.ptr(i, 0);
		for (int j = 0; j < src.getWidth(); ++j) {
			int s = 0;
			auto *lpsrc = psrc - start_offset;
			auto *pconv = start_pconv;
			for (int ii = 0; ii < conv.getHeight(); ++ii) {
				for (int jj = 0; jj < conv.getWidth(); ++jj) {
					s += static_cast<int>(*lpsrc) * *pconv;
					++lpsrc, ++pconv;
				}
				lpsrc += next_row_offset;
			}
			*pdst = s / divisor;
			++pdst;
			++psrc;
		}
	}
	return dst;
}

static Image<Px_16s> applyLinearConvultion_mt(const Image<Px_16s> &src, const std::vector<int> &conv, int divisor, int stride, int y1, int x1, int y2, int x2)
{
	if (conv.size() % 2 == 0) {
		throw std::runtime_error("Size of convultion kernel must be odd.");
	}
	Image<Px_16s> dst = createSimilarImage<Px_16s>(src);
	int rad = conv.size() / 2;
	int d = 2 * rad + 1;
	std::vector<std::thread> active_threads;
	int threads = std::thread::hardware_concurrency();
	for (int t = 0; t < threads; ++t) {
		active_threads.push_back(std::thread([=, &src, &dst, &conv](int f, int t, int step) {
			for (int i = f; i < t; i += step) {
				const Px_16s *psrc = src.ptr(i, x1);
				Px_16s *pdst = dst.ptr(i, x1);
				for (int j = x1; j < x2; ++j) {
					const Px_16s *p = psrc;
					p -= stride * rad;
					int sum = 0;
					for (int k = 0; k < d; ++k) {
						sum += *p * conv[k];
						p += stride;
					}
					*pdst = sum / divisor;
					++psrc;
					++pdst;
				}
			}
		}, y1 + t, y2, threads));
	}
	std::for_each(active_threads.begin(), active_threads.end(), [](std::thread &t) {
		t.join();
	});
	return dst;
}

static Image<Px_16s> applyLinearConvultion(const Image<Px_16s> &src, const std::vector<int> &conv, int divisor, int stride, int y1, int x1, int y2, int x2)
{
#ifndef NO_MT
	if (std::thread::hardware_concurrency() > 1) {
		try {
			return applyLinearConvultion_mt(src, conv, divisor, stride, y1, x1, y2, x2);
		}
      	catch (std::exception &ex) {
          	std::cerr << "MT error: " << ex.what() << std::endl;
       	}
	}
#endif
	if (conv.size() % 2 == 0) {
		throw std::runtime_error("Size of convultion kernel must be odd.");
	}
	Image<Px_16s> dst = createSimilarImage<Px_16s>(src);
	int rad = conv.size() / 2;
	int d = 2 * rad + 1;
	for (int i = y1; i < y2; ++i) {
		const Px_16s *psrc = src.ptr(i, x1);
		Px_16s *pdst = dst.ptr(i, x1);
		for (int j = x1; j < x2; ++j) {
			const Px_16s *p = psrc;
			p -= stride * rad;
			int sum = 0;
			for (int k = 0; k < d; ++k) {
				sum += *p * conv[k];
				p += stride;
			}
			*pdst = sum / divisor;
			++psrc;
			++pdst;
		}
	}
	return dst;
}

Image<Px_16s> applyVertConvultion(const Image<Px_16s> &src, const std::vector<int> &conv, int divisor)
{
	return applyLinearConvultion(src, conv, divisor, src.getStride(), 
								 0, -static_cast<int>(conv.size() / 2), 
								 src.getHeight(), src.getWidth() + static_cast<int>(conv.size() / 2));
}

Image<Px_16s> applyHorConvultion(const Image<Px_16s> &src, const std::vector<int> &conv, int divisor)
{
	return applyLinearConvultion(src, conv, divisor, 1,
								 -static_cast<int>(conv.size() / 2), 0,
								 src.getHeight() + static_cast<int>(conv.size() / 2), src.getWidth());
}
