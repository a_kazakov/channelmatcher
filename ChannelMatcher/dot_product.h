#pragma once

#include <cstdint>
#include <iostream>

#include <xmmintrin.h>
#include <emmintrin.h>
#include <pmmintrin.h>
#include <immintrin.h>

#ifdef _MSC_VER
#include <intrin.h>
#else
#define __cpuid(r,func)\
	__asm__ __volatile__ ("cpuid":\
	"=a" (r[0]), "=b" (r[1]), "=c" (r[2]), "=d" (r[3]) : "a" (func));
#endif

inline bool support_sse2()
{
	static int sv = -1;
	if (sv == -1) {
		//sv = _may_i_use_cpu_feature(_FEATURE_SSE3);
		int res[4];
		__cpuid(res, 1);
		sv = (res[3] & 0x04000000) != 0;
	}
	return !!sv;
}

inline int dot_product_16_nosse(const int16_t *a, const int16_t *b, size_t size)
{
	std::cerr << "Running in slow mode..." << std::endl;
	int res = 0;
	while (size--) {
		res += static_cast<int>(*a) * *b;
		++a, ++b;
	}
	return res;
}

inline int32_t dot_product_16(const int16_t *a, const int16_t *b, size_t size)
{
	if (!support_sse2()) {
		return dot_product_16_nosse(a, b, size);
	}
#ifdef _MSC_VER
	__declspec(align(16)) int32_t buf[4] ={ 0 };
	__declspec(align(16)) int16_t l1[8] ={ 0 }, l2[8] ={ 0 };
#else
	int32_t buf[4] __attribute__ ((aligned (16))) = { 0 };
	int16_t  l1[8] __attribute__ ((aligned (16))) = { 0 };
	int16_t  l2[8] __attribute__ ((aligned (16))) = { 0 };
#endif
	__m128i *buf128 = reinterpret_cast<__m128i *>(buf);
	__m128i t1, t2, tr, ts = *reinterpret_cast<__m128i *>(buf);
	const __m128i *ai = reinterpret_cast<const __m128i *>(a);
	const __m128i *bi = reinterpret_cast<const __m128i *>(b);
	register int nsize = size >> 3; // size / 8
	int nsize2 = nsize << 3;
	while (nsize--) {
		t1 = _mm_loadu_si128(ai);
		t2 = _mm_loadu_si128(bi);
		tr = _mm_madd_epi16(t1, t2);
		ts = _mm_add_epi32(ts, tr);
		++ai, ++bi;
	}
	*buf128 = ts;
	int res = buf[0] + buf[1] + buf[2] + buf[3];
	for (size_t i = nsize2; i < size; ++i) {
		l1[i - nsize2] = a[i];
		l2[i - nsize2] = b[i];
	}
	*buf128 = _mm_madd_epi16(*reinterpret_cast<__m128i *>(l1), *reinterpret_cast<__m128i *>(l2));
	res += buf[0] + buf[1] + buf[2] + buf[3];
	return res;
}
