#include "sobel.h"

Image<Px_16s> sobel_y(const Image<Px_16s> &src)
{
	src.checkBorders(2);
	src.fillBorders();
	Image<int> k(3, 3);
	int rk[9] ={ 1, 2, 1, 0, 0, 0, -1, -2, -1 };
	std::memcpy(k.ptr(0, 0), rk, 9 * sizeof(int));
	return applyConvultion(src, k);
}

Image<Px_16s> sobel_x(const Image<Px_16s> &src)
{
	src.checkBorders(2);
	src.fillBorders();
	Image<int> k(3, 3);
	int rk[9] = { -1, 0, 1, -2, 0, 2, -1, 0, 1 };
	std::memcpy(k.ptr(0, 0), rk, 9 * sizeof(int));
	return applyConvultion(src, k);
}

void sobel(Image<Px_16s> &out_g, Image<float> &out_a, const Image<Px_16s> &src)
{
	Image<Px_16s> res_g = createSimilarImage<Px_16s>(src);
	Image<float> res_a = createSimilarImage<float>(src);
	Image<Px_16s> tmp_y = sobel_y(src);
	Image<Px_16s> tmp_x = sobel_x(src);
	for (int i = 0; i < src.getHeight(); ++i) {
		Px_16s *py = tmp_y.ptr(i, 0);
		Px_16s *px = tmp_x.ptr(i, 0);
		Px_16s *pg = res_g.ptr(i, 0);
		float *pa = res_a.ptr(i, 0);
		for (int j = 0; j < src.getWidth(); ++j) {
			*pg = static_cast<Px_16s>(std::sqrt(static_cast<float>(*py * *py + *px * *px)));
			*pa = std::atan2(static_cast<float>(*py), static_cast<float>(*px));
			++py, ++px, ++pg, ++pa;
		}
	}
	out_g = res_g;
	out_a = res_a;
}

Image<Px_16s> sobel_map(const Image<Px_16s> &src)
{
	Image<Px_16s> res_g = createSimilarImage<Px_16s>(src);
	Image<Px_16s> tmp_y = sobel_y(src);
	Image<Px_16s> tmp_x = sobel_x(src);
	for (int i = 0; i < src.getHeight(); ++i) {
		Px_16s *py = tmp_y.ptr(i, 0);
		Px_16s *px = tmp_x.ptr(i, 0);
		Px_16s *pg = res_g.ptr(i, 0);
		for (int j = 0; j < src.getWidth(); ++j) {
			*pg = static_cast<Px_16s>(std::sqrt(static_cast<float>(*py * *py + *px * *px)));
			++py, ++px, ++pg;
		}
	}
	return res_g;
}
