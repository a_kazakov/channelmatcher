#pragma once

#include <vector>
#include <thread>
#include <algorithm>

#include "dot_product.h"

#include "Image.h"

Image<Px_16s> applyConvultion(const Image<Px_16s> &src, const Image<int> &conv, int divisor = 1);
Image<Px_16s> applyVertConvultion(const Image<Px_16s> &src, const std::vector<int> &conv, int divisor = 1);
Image<Px_16s> applyHorConvultion(const Image<Px_16s> &src, const std::vector<int> &conv, int divisor = 1);
