#pragma once

#include "Image.h"

#include "channels_operations.h"
#include "img_operations.h"

#include "convultions.h"

void sharpen(Image<Px_8u3c> &img);
