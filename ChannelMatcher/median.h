#pragma once

#include <vector>

#include "Image.h"

enum MedianFilterType
{
	MEDIAN_SIMPLE, MEDIAN_LINEAR, MEDIAN_CONST
};

Image<Px_8u> median(const Image<Px_8u> &img, int rad, MedianFilterType type);
