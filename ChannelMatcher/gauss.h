#pragma once

#include <vector>

#include "Image.h"

#include "convultions.h"

Image<Px_16s> gauss(const Image<Px_16s> &src, float sigma, int rad = -1);
Image<Px_16s> slowGauss(const Image<Px_16s> &src, float sigma, int rad = -1);
