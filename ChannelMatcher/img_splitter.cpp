#include "img_splitter.h"

static inline bool check_limits(const std::pair<int, int> &res)
{
	if (res.first == INT_MAX) {
		return false;
	}
	return true;
}

static inline void update_limits(std::pair<int, int> &l, int v) {
	l.first  = std::min(l.first, v);
	l.second = std::max(l.second, v);
};

static std::pair<int, int> process_hist(const std::vector<int> &hist)
{
	std::pair<int, int> res = std::make_pair(INT_MAX, INT_MIN);
	std::vector<std::pair<int, int>> maximums;
	// code
	for (size_t i = 0; i < hist.size(); ++i) {
		maximums.push_back(std::make_pair(-hist[i], i));
	}
	std::sort(maximums.begin(), maximums.end());
	int first_s = -maximums[0].first;
	for (size_t i = 0; i < maximums.size() / 30 || !check_limits(res); ++i) {
		int v = maximums[i].second;
		if (-maximums[i].first < first_s / 4 && check_limits(res)) {
			break;
		}
		update_limits(res, v);
	}
	return res;
}

static void crop_vert(Image<Px_8u> &orig, Image<Px_16s> &img, int y1, int y2)
{
	// crop guide vertically
	int bases[2] = {
		0, img.getWidth() / 20 * 19
	};
	int sizes[2] = {
		img.getWidth() / 20, img.getWidth() / 20
	};
//	Image<Px_16s> cropped[4];
	Image<Px_8u> guides[2];
	for (int i = 0; i < 2; ++i) {
		img.setRoi(0, bases[i], img.getHeight(), sizes[i]);
		guides[i] = canny(img);
	}

	// calculate histogram
	std::vector<int> hist[4];
	for (int k = 0; k < 2; ++k) {
		for (int j = 0; j < guides[k].getWidth(); ++j) {
			int sum = 0;
			for (int i = 0; i < guides[k].getHeight(); ++i) {
				sum += !!guides[k](i, j);
			}
			hist[k].push_back(sum);
		}
	}

	// analyse histogram
	std::vector<std::pair<int, int>> res_h(2);
	for (int i = 0; i < 2; ++i) {
		res_h[i] = process_hist(hist[i]);
		res_h[i].first += bases[i];
		res_h[i].second += bases[i];
	}

	// set result
	orig.setRoi(y1 + 1, res_h[0].second + 1, y2 - y1 - 1, res_h[1].first - res_h[0].second - 1);

	// clean after ourselves
	img.resetRoi();
};

void unstackChannels(Image<Px_8u> &out1, Image<Px_8u> &out2, Image<Px_8u> &out3, const Image<Px_8u> &src)
{
	Image<Px_16s> img = to16bit(src);
	Image<Px_8u> orig;
	orig.assign(src);
	int bases[4] = {
		2, img.getHeight() / 30 * 9, img.getHeight() / 30 * 19, img.getHeight() / 30 * 29 - 2
	};
	int sizes[4] ={
		img.getHeight() / 30, img.getHeight() / 30 * 2, img.getHeight() / 30 * 2, img.getHeight() / 30
	};
	Image<Px_8u> guides[4];
	for (int i = 0; i < 4; ++i) {
		img.setRoi(bases[i] - 2, 0, sizes[i] + 4, img.getWidth());
		guides[i] = canny(img);
		guides[i].setRoi(2, 0, sizes[i], guides[i].getWidth());
	}

	// create histogram
	std::vector<int> hist[4];
	for (int k = 0; k < 4; ++k) {
		for (int i = 0; i < guides[k].getHeight(); ++i) {
			int sum = 0;
			for (int j = 0; j < guides[k].getWidth(); ++j) {
				sum += !!guides[k](i, j);
			}
			hist[k].push_back(sum);
		}
	}
	// analyse histogram
	std::vector<std::pair<int, int>> res_v(4);
	for (int i = 0; i < 4; ++i) {
		res_v[i] = process_hist(hist[i]);
		res_v[i].first += bases[i];
		res_v[i].second += bases[i];
	}

	// save result
	crop_vert(orig, img, res_v[0].second, res_v[1].first);
	out1 = orig.createFromROI();
	crop_vert(orig, img, res_v[1].second, res_v[2].first);
	out2 = orig.createFromROI();
	crop_vert(orig, img, res_v[2].second, res_v[3].first);
	out3 = orig.createFromROI();
}