#include "ImgMatcher.h"

class ImgMatcher
{
public:
	ImgMatcher(const Image<Px_16s> &src1_p, const Image<Px_16s> &src2_p, 
			   int base_y, int base_x, int radius);
	std::pair<int, int> matchChannels();
protected:
	Image<Px_16s> src1, src2;
	Image<int64_t> sq2;
	int64_t sq1_v;
private:
	std::pair<int, int> matchChannels_mt();
	inline int64_t getRsq(const Image<int64_t> &src, int y, int x, int h, int w) const;
	inline int64_t findSSD(int y, int x, int h, int w) const;
	Image<int64_t> createSq(const Image<Px_16s> &src);
	inline int64_t findSqv(const Image<Px_16s> &src);
	int base_y, base_x, radius;
};

std::pair<int, int> run_ImgMatcher(const Image<Px_16s> &src1_p, const Image<Px_16s> &src2_p,
								  int base_y, int base_x, int radius)
{
	ImgMatcher im(src1_p, src2_p, base_y, base_x, radius);
	return im.matchChannels();
}

ImgMatcher::ImgMatcher(const Image<Px_16s> &src1_p, const Image<Px_16s> &src2_p, int base_y, int base_x, int radius)
{
	src1 = src1_p;
	src2 = src2_p;
	int req_border = std::max(std::abs(base_y), std::abs(base_x)) +
	                 std::max(std::abs(src1.getWidth() - src2.getWidth()),
	                          std::abs(src1.getHeight() - src2.getHeight())) + radius + 10;
	src1.checkBorders(req_border);
	src2.checkBorders(req_border);
	src1.fillBorders();
	src2.fillBorders();
	sq1_v = findSqv(src1);
	sq2 = createSq(src2);
	this->radius = radius;
	this->base_y = base_y;
	this->base_x = base_x;
}

std::pair<int, int> ImgMatcher::matchChannels()
{
#ifndef NO_MT
	if (std::thread::hardware_concurrency() > 1) {
		return matchChannels_mt();
	}
#endif
	int my = 0, mx = 0;
	int64_t mv = 1000000000000000000;
	for (int i = -radius; i <= radius; ++i) {
		for (int j = -radius; j <= radius; ++j) {
			int64_t v = findSSD(base_y + i, base_x + j, src1.getHeight(), src1.getWidth());
			if (v < mv) {
				mv = v;
				my = base_y + i;
				mx = base_x + j;
			}
		}
	}
	return std::make_pair(my, mx);
}

std::pair<int, int> ImgMatcher::matchChannels_mt()
{
	int my = 0, mx = 0;
	int64_t mv = 1000000000000000000;
	std::vector<std::pair<int, int>> shifts;
	for (int i = -radius; i <= radius; ++i) {
		for (int j = -radius; j <= radius; ++j) {
			shifts.push_back(std::make_pair(i, j));
		}
	}
	std::mutex mtx;
	std::vector<std::thread> active_threads;
	int threads = std::thread::hardware_concurrency();
	for (int t = 0; t < threads; ++t) {
		active_threads.push_back(std::thread([=, &mtx, &my, &mx, &mv, &shifts](int f, int t, int step) {
			for (int i = f; i < t; i += step) {
				std::pair<int, int> &s = shifts[i];
				int64_t v = findSSD(base_y + s.first, base_x + s.second, src1.getHeight(), src1.getWidth());
				mtx.lock();
				if (v < mv) {
					mv = v;
					my = base_y + s.first;
					mx = base_x + s.second;
				}
				mtx.unlock();
			}
		}, t, shifts.size(), threads));
	}
	std::for_each(active_threads.begin(), active_threads.end(), [](std::thread &t) {
		t.join();
	});
	return std::make_pair(my, mx);
}

int64_t ImgMatcher::getRsq(const Image<int64_t> &src, int y, int x, int h, int w) const
{
	// REQUIRES 1px BORDER!
	return src(y + h - 1, x + w - 1) + src(y - 1, x - 1) - src(y + h - 1, x - 1) - src(y - 1, x + w - 1);
}

int64_t ImgMatcher::findSSD(int y, int x, int h, int w) const
{
	int64_t s1 = sq1_v;
	int64_t s2 = getRsq(sq2, y, x, h, w);
	int64_t sm = 0;

	for (int i = 0; i < h; ++i) {
		const Px_16s *p1 = src1.ptr(i, 0);
		const Px_16s *p2 = src2.ptr(i + y, x);
		sm += dot_product_16(p1, p2, w);
	}
	return s1 + s2 - 2 * sm;
}

Image<int64_t> ImgMatcher::createSq(const Image<Px_16s> &src)
{
	Image<int64_t> dst = createSimilarImage<int64_t>(src);
	int total_pixels = src.getNumPixels();
	const Px_16s *psrc = src.getFirstPtr();
	int64_t *pdst = dst.getFirstPtr();
	for (int i = 0; i < total_pixels; ++i) {
		int64_t p = static_cast<int64_t>(*psrc) * static_cast<int64_t>(*psrc);
		// calculate RSQ
		if (i < src.getStride()) {
			if (i % src.getStride() == 0) {
				*pdst = p; // top left corner
			} else {
				*pdst = *(pdst - 1) + p; // top line
			}
		} else {
			if (i % src.getStride() == 0) {
				*pdst = *(pdst - dst.getStride()) + p; // left column
			} else {
				*pdst = *(pdst - dst.getStride()) + *(pdst - 1) - *(pdst - dst.getStride() - 1) + p;
			}
		}
		++psrc, ++pdst;
	}
	return dst;
}

int64_t ImgMatcher::findSqv(const Image<Px_16s> &src)
{
	int64_t res = 0;
	for (int i = 0; i < src.getHeight(); ++i) {
		res += dot_product_16(src.ptr(i, 0), src.ptr(i, 0), src.getWidth());
	}
	return res;
}
