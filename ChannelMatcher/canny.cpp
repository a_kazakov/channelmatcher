#include "canny.h"

static void run_wave(Image<Px_16s> &borders, Image<bool> &visited, int y, int x, int l1, int l2)
{
	std::queue<std::pair<int, int>> q;
	q.push(std::make_pair(y, x));
	visited(y, x) = true;
	int offsets_y[8] ={ 0, 1, 0, -1, 1, 1, -1, -1 };
	int offsets_x[8] ={ 1, 0, -1, 0, 1, -1, 1, -1 };

	while (!q.empty()) {
		auto cur = q.front();
		q.pop();
		for (int i = 0; i < 8; ++i) {
			int y = cur.first + offsets_y[i];
			int x = cur.second + offsets_x[i];
			if (!(0 <= y && y < borders.getHeight() && 0 <= x && x < borders.getWidth())) {
				continue;
			} else if (visited(y, x)) {
				continue;
			} else if (borders(y, x) < l1) {
				continue;
			}
			visited(y, x) = true;
			q.push(std::make_pair(y, x));
			borders(y, x) = l2;
		}
	}
}

static inline void modify_angle(int &py, int &px, int y, int x)
{
	if (100000 * std::abs(x) < 41421 * std::abs(y)) {
		py = 1, px = 0;
	} else if (100000 * std::abs(y) < 41421 * std::abs(x)) {
		py = 0, px = 1;
	} else if (x * y < 0) {
        py = px = 1;
    } else {
        py = 1, px = -1;
	}
}

Image<Px_8u> canny(const Image<Px_16s> &src, int l1, int l2)
{
	// 1) gauss filter
	Image<Px_16s> blured = gauss(src, 1.4f, 2);

	// 2) sobel filter
	Image<Px_16s> borders_y = sobel_y(blured);
	Image<Px_16s> borders_x = sobel_x(blured);

	// 3) leave maximums
	Image<Px_16s> borders = createSimilarImage<Px_16s>(borders_y);

	for (int i = 0; i < borders.getHeight(); ++i) {
		for (int j = 0; j < borders.getWidth(); ++j) {
			borders(i, j) = static_cast<int>(std::sqrt(borders_y(i, j) * borders_y(i, j) + 
			                                           borders_x(i, j) * borders_x(i, j)));
		}
	}

	Image<Px_16s> new_borders = createSimilarImage<Px_16s>(borders);

	borders.checkBorders(1);
	borders.fillBorders();

	for (int i = 0; i < borders.getHeight(); ++i) {
		auto *nb = new_borders.ptr(i, 0);
		auto *bptr = borders.ptr(i, 0);
		for (int j = 0; j < borders.getWidth(); ++j) {
			int py, px;
			modify_angle(py, px, borders_y(i, j), borders_x(i, j));

			int16_t p1 = borders(i + py, j + px);
			int16_t p2 = borders(i - py, j - px);
	
			if (*bptr < p1 || *bptr <= p2) {
				*nb = -1;
			} else {
				*nb = borders(i, j);
			}
			++nb, ++bptr;
		}
	}

	borders = new_borders;

	// 4) run wave
	Image<bool> visited(borders.getHeight(), borders.getWidth());
	std::memset(visited.getFirstPtr(), 0, visited.getNumPixels() * sizeof(bool));

	for (int i = 0; i < borders.getHeight(); ++i) {
		auto *pbrd = borders.ptr(i, 0);
		auto *pvis = visited.ptr(i, 0);
		for (int j = 0; j < borders.getWidth(); ++j) {
			if (*pbrd >= l2) {
				if (!*pvis) {
					run_wave(borders, visited, i, j, l1, l2);
				}
			}
			++pbrd, ++pvis;
		}
	}

	// 5) finalize result
	Image<Px_8u> res(src.getHeight(), src.getWidth(), 0);
	for (int i = 0; i < borders.getHeight(); ++i) {
		auto *pres = res.ptr(i, 0);
		auto *pbrd = borders.ptr(i, 0);
		for (int j = 0; j < borders.getWidth(); ++j) {
			*pres = 255 * (*pbrd >= l2);
			++pres, ++pbrd;
		}
	}

	return res;
}