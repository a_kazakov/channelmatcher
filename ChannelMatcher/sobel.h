#pragma once

#include "Image.h"

#include "convultions.h"

Image<Px_16s> sobel_y(const Image<Px_16s> &img);
Image<Px_16s> sobel_x(const Image<Px_16s> &img);
void sobel(Image<Px_16s> &out_g, Image<float> &out_a, const Image<Px_16s> &img);
Image<Px_16s> sobel_map(const Image<Px_16s> &img);
