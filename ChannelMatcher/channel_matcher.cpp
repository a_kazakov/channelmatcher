#include "channel_matcher.h"

std::pair<int, int> findShift(const Image<Px_16s> &img1, const Image<Px_16s> &img2)
{
	if (img1.getHeight() > 400 || img1.getWidth() > 400) {
		Image<Px_16s> resampled1 = resizeImage(img1, img1.getHeight() / RESAMPLE_RATIO,
											   img1.getWidth() / RESAMPLE_RATIO);
		Image<Px_16s> resampled2 = resizeImage(img2, img2.getHeight() / RESAMPLE_RATIO,
											   img2.getWidth() / RESAMPLE_RATIO);
		std::pair<int, int> base_shift = findShift(resampled1, resampled2);
		base_shift.first *= RESAMPLE_RATIO;
		base_shift.second *= RESAMPLE_RATIO;
		return run_ImgMatcher(img1, img2, base_shift.first, base_shift.second, 3);
	} else {
		return run_ImgMatcher(img1, img2);
	}
}

Image<Px_16s> boxFilter(const Image<Px_16s> &src, int rad) {
	if (rad == 0) {
		return src;
	}
	src.checkBorders(rad);
	src.fillBorders();
	auto tmp = createSimilarImage<Px_16s>(src);
	auto res = createSimilarImage<Px_16s>(src);
	int d = 2 * rad + 1;
	for (int i = 0; i < src.getHeight(); ++i) {
		int sum = 0;
		auto psrc = src.ptr(i, -rad);
		auto ptmp = tmp.ptr(i, 0);
		for (int jj = -rad; jj < rad; ++jj) {
			sum += *psrc;
			++psrc;
		}
		for (int j = 0; j < src.getWidth(); ++j) {
			sum += *psrc;
			*ptmp = static_cast<Px_16s>(sum / d);
			sum -= *(psrc - d + 1);
			++ptmp, ++psrc;
		}
	}
	for (int j = 0; j < src.getWidth(); ++j) {
		int sum = 0;
		auto ptmp = tmp.ptr(-rad, j);
		auto pres = res.ptr(0, j);
		for (int ii = -rad; ii < rad; ++ii) {
			sum += *ptmp;
			ptmp += tmp.getStride();
		}
		for (int i = 0; i < src.getHeight(); ++i) {
			sum += *ptmp;
			*pres= static_cast<Px_16s>(sum / d);
			sum -= *(ptmp - tmp.getStride() * (d - 1));
			ptmp += tmp.getStride(), pres += res.getStride();
		}
	}
	return res;
}

Image<Px_8u3c> matchChannels(const Image<Px_8u> &src, int subpixel)
{
	Image<Px_8u> c1, c2, c3;
	// Split channels
	unstackChannels(c1, c2, c3, src);
	// Find 1st vector
	Image<Px_16s> c1s = sobel_map(boxFilter(to16bit(c1), src.getWidth() / 500));
	Image<Px_16s> c2s = sobel_map(boxFilter(to16bit(c2), src.getWidth() / 500));
	Image<Px_16s> c3s = sobel_map(boxFilter(to16bit(c3), src.getWidth() / 500));

	if (subpixel == 1) {
		std::pair<int, int> res12 = findShift(c1s, c2s);
		std::pair<int, int> res13 = findShift(c1s, c3s);
		Rectangle rect(0, 0, c1.getHeight(), c1.getWidth());
		rect *= Rectangle(-res12.first, -res12.second, c2.getHeight(), c2.getWidth());
		rect *= Rectangle(-res13.first, -res13.second, c3.getHeight(), c3.getWidth());

		c1.setRoi(rect.y, rect.x, rect.h, rect.w);
		c2.setRoi(res12.first + rect.y, res12.second + rect.x, rect.h, rect.w);
		c3.setRoi(res13.first + rect.y, res13.second + rect.x, rect.h, rect.w);
	} else {
		auto c1s_big = resizeImage(c1s, subpixel * c1s.getHeight(), subpixel * c1s.getWidth());
		auto c2s_big = resizeImage(c2s, subpixel * c2s.getHeight(), subpixel * c2s.getWidth());
		auto c3s_big = resizeImage(c3s, subpixel * c3s.getHeight(), subpixel * c3s.getWidth());
		c1 = resizeImage(c1, subpixel * c1.getHeight(), subpixel * c1.getWidth());
		c2 = resizeImage(c2, subpixel * c2.getHeight(), subpixel * c2.getWidth());
		c3 = resizeImage(c3, subpixel * c3.getHeight(), subpixel * c3.getWidth());

		std::pair<int, int> res12_s = findShift(c1s_big, c2s_big);
		std::pair<int, int> res13_s = findShift(c1s_big, c3s_big);

		Rectangle rect(0, 0, c1.getHeight(), c1.getWidth());
		rect *= Rectangle(-res12_s.first, -res12_s.second, c2.getHeight(), c2.getWidth());
		rect *= Rectangle(-res13_s.first, -res13_s.second, c3.getHeight(), c3.getWidth());

		c1.setRoi(rect.y, rect.x, rect.h, rect.w);
		c2.setRoi(res12_s.first + rect.y, res12_s.second + rect.x, rect.h, rect.w);
		c3.setRoi(res13_s.first + rect.y, res13_s.second + rect.x, rect.h, rect.w);

		c1 = resizeImage(c1, c1.getHeight() / subpixel, c1.getWidth() / subpixel);
		c2 = resizeImage(c2, c2.getHeight() / subpixel, c2.getWidth() / subpixel);
		c3 = resizeImage(c3, c3.getHeight() / subpixel, c3.getWidth() / subpixel);
	}

//	c1.save("c1.bmp");
//	c2.save("c2.bmp");
//	c3.save("c3.bmp");

	return mixChannels(c1, c2, c3);
}
