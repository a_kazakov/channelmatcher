#pragma once

#include <vector>

#include "constants.h"

#include "Image.h"

#include "img_operations.h"
#include "channels_operations.h"

void autoContrast(Image<Px_8u3c> &img, float fraction);
