#include <algorithm>
#include <string>
#include <vector>
#include <unordered_map>

class ArgParser
{
public:
	ArgParser(int argc, char *argv[]);
	int getSize(const std::string &name = "");
	char *getParam(int n, const std::string &name = "");
protected:
	int argc;
	char **argv;
	std::unordered_map<std::string, std::pair<int, int>> stops;
};
