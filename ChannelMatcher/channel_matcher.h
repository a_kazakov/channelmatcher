#pragma once

#include "Image.h"

#include "constants.h"

#include "Rectangle.h"

#include "channels_operations.h"
#include "img_operations.h"

#include "gauss.h"
#include "sobel.h"
#include "canny.h"
#include "median.h"
#include "img_splitter.h"

#include "ImgMatcher.h"

Image<Px_8u3c> matchChannels(const Image<Px_8u> &src, int subpixel = 1);
