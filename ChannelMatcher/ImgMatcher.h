#pragma once

#include <utility>
#include <numeric>
#include <thread>
#include <mutex>

#ifdef _MSC_VER
#include <ppl.h>
#else
#include <parallel/algorithm>
#endif

#include "dot_product.h"

#include "constants.h"

#include "Image.h"

#include "channels_operations.h"

#include "gauss.h"
#include "sobel.h"

std::pair<int, int> run_ImgMatcher(const Image<Px_16s> &src1_p, const Image<Px_16s> &src2_p,
								   int base_y = 0, int base_x = 0, int radius = SHIFT_RADIUS);
