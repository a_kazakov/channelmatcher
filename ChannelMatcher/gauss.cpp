#include "gauss.h"

Image<Px_16s> gauss(const Image<Px_16s> &src, float sigma, int rad)
{
	rad = rad < 0 ? static_cast<int>(3 * sigma + 0.9) : rad;
	src.checkBorders(rad + 1);
	src.fillBorders();
	std::vector<int> kernel(2 * rad + 1);
	int sum = 0;
	for (int i = 0; i <= rad; ++i) {
		int v = static_cast<int>(1000 * std::exp(-(i * i) / (2 * sigma * sigma)));
		sum += (i == 0 ? v : 2 * v);
		kernel[rad - i] = kernel[rad + i] = v;
	}
	Image<Px_16s> tmp = applyVertConvultion(src, kernel, sum);
	tmp = applyHorConvultion(tmp, kernel, sum);
	return tmp;
}

Image<Px_16s> slowGauss(const Image<Px_16s> &src, float sigma, int rad)
{
	rad = rad < 0 ? static_cast<int>(3 * sigma + 0.9) : rad;
	src.checkBorders(rad + 1);
	src.fillBorders();
	Image<int> kernel(2 * rad + 1, 2 * rad + 1);
	int sum = 0;
	for (int i = 0; i <= rad; ++i) {
		for (int j = 0; j <= rad; ++j) {
			int v = static_cast<int>(1000 * std::exp(-(i * i + j * j) / (2 * sigma * sigma)));
			sum += v * (1 + (i != 0) + (j != 0) + (i != 0 && j != 0));
			kernel(rad - i, rad - j) =
				kernel(rad - i, rad + j) =
				kernel(rad + i, rad - j) =
				kernel(rad + i, rad + j) = v;
		}
	}
	return applyConvultion(src, kernel, sum);
}
