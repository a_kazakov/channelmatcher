#include "median.h"

struct Histogram {
	static const int n = 256;
	static const int ns = 16;
	int data[n];
	int data_s[ns];
	int data_size;
	Histogram()
	{
		clear();
	}
	void clear()
	{
		data_size = 0;
		std::memset(data, 0, n * sizeof(int));
		std::memset(data_s, 0, ns * sizeof(int));
	}
	int getMedian()
	{
		int s = 0;
/*		for (int i = 0; i < n; ++i) {
			s += data[i];
			if (s > data_size / 2) {
				return i;
			}
		}*/
		for (int i = 0; i < ns; ++i) {
			s += data_s[i];
			if (s > data_size / 2) {
				for (int j = ns * i + ns - 1;; --j) {
					s -= data[j];
					if (s <= data_size / 2) {
						return j;
					}
				}
			}
		}
		return 0;
	}
	Histogram &operator += (const Histogram &h)
	{
		data_size += h.data_size;
		for (int i = 0; i < n; ++i) {
			data[i] += h.data[i];
		}
		for (int i = 0; i < ns; ++i) {
			data_s[i] += h.data_s[i];
		}
		return *this;
	}
	Histogram &operator -= (const Histogram &h)
	{
		data_size -= h.data_size;
		for (int i = 0; i < n; ++i) {
			data[i] -= h.data[i];
		}
		for (int i = 0; i < ns; ++i) {
			data_s[i] -= h.data_s[i];
		}
		return *this;
	}
	Histogram &operator += (int x)
	{
		++data_size;
		++data[x];
		++data_s[x / ns];
		return *this;
	}
	Histogram &operator -= (int x)
	{
		--data_size;
		--data[x];
		--data_s[x / ns];
		return *this;
	}
};

static Image<Px_8u> median_simple(const Image<Px_8u> &img, int rad)
{
	Histogram hist;
	Image<Px_8u> res = createSimilarImage<Px_8u>(img);
	for (int i = 0; i < img.getHeight(); ++i) {
		for (int j = 0; j < img.getWidth(); ++j) {
			hist.clear();
			for (int ii = -rad; ii <= rad; ++ii) {
				for (int jj = -rad; jj <= rad; ++jj) {
					hist += img(i + ii, j + jj);
				}
			}
			res(i, j) = hist.getMedian();
		}
	}
	return res;
}

static Image<Px_8u> median_linear(const Image<Px_8u> &img, int rad)
{
	Histogram hist;
	Image<Px_8u> res = createSimilarImage<Px_8u>(img);
	for (int i = 0; i < img.getHeight(); ++i) {
		hist.clear();
		for (int ii = -rad; ii <= rad; ++ii) {
			for (int jj = -rad; jj <= rad - 1; ++jj) {
				hist += img(i + ii, jj);
			}
		}
		for (int j = 0; j < img.getWidth(); ++j) {
			for (int ii = -rad; ii <= rad; ++ii) {
				hist += img(i + ii, j + rad);
			}
			res(i, j) = hist.getMedian();
			for (int ii = -rad; ii <= rad; ++ii) {
				hist -= img(i + ii, j - rad);
			}
		}
	}
	return res;
}

static Image<Px_8u> median_const(const Image<Px_8u> &img, int rad)
{
	Histogram hist;
	std::vector<Histogram> buckets;
	Image<Px_8u> res = createSimilarImage<Px_8u>(img);
	
	buckets.resize(img.getWidth() + 2 * rad);
	for (int i = -rad; i <= rad - 1; ++i) {
		for (int j = -rad; j < img.getWidth() + rad; ++j) {
			buckets[j + rad] += img(i, j);
		}
	}
	for (int i = 0; i < img.getHeight(); ++i) {
		// add pixels to buckets
		for (int j = -rad; j < img.getWidth() + rad; ++j) {
			buckets[j + rad] += img(i + rad, j);
		}
		// init moving hist
		hist.clear();
		for (int jj = -rad; jj <= rad - 1; ++jj) {
			hist += buckets[jj + rad];
		}
		// process row
		for (int j = 0; j < img.getWidth(); ++j) {
			hist += buckets[j + 2 * rad];
			res(i, j) = hist.getMedian();
			hist -= buckets[j]; // j - rad + rad
		}
		// remove pixels from buckets
		for (int j = -rad; j < img.getWidth() + rad; ++j) {
			buckets[j + rad] -= img(i - rad, j);
		}
	}
	return res;
}

Image<Px_8u> median(const Image<Px_8u> &img, int rad, MedianFilterType type)
{
	img.checkBorders(rad + 1);
	img.fillBorders();
	switch (type) {
	case MEDIAN_SIMPLE:
		return median_simple(img, rad);
	case MEDIAN_LINEAR:
		return median_linear(img, rad);
	case MEDIAN_CONST:
		return median_const(img, rad);
	default:
		throw std::runtime_error("Unknown type median filter.");
	}
}
