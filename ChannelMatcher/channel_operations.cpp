#include "channels_operations.h"

Image<Px_8u> toGrayscale(const Image<Px_8u3c> &src)
{
	Image<Px_8u> dst = createSimilarImage<Px_8u>(src);
	int total_pixels    = src.getNumPixels();
	const Px_8u3c *psrc = src.getFirstPtr();
	Px_8u *pdst         = dst.getFirstPtr();
	for (int i = 0; i < total_pixels; ++i) {
		*pdst = static_cast<uint8_t>(
			(
				 721 * static_cast<uint32_t>((*psrc)[0]) +
				7154 * static_cast<uint32_t>((*psrc)[1]) +
				2125 * static_cast<uint32_t>((*psrc)[2])
			) / 10000
		);
		++psrc;
		++pdst;
	}
	return dst;
}

Image<Px_32s> to32bit(const Image<Px_8u> &src)
{
	return convertImgType<Px_8u, Px_32s>(src);
}

Image<Px_16s> to16bit(const Image<Px_8u> &src)
{
	return convertImgType<Px_8u, Px_16s>(src);
}

Image<Px_8u> to8bit(const Image<Px_16s> &src, int min, int max)
{
	Image<Px_8u> dst = createSimilarImage<Px_8u, Px_16s>(src);
	int total_pixels   = src.getNumPixels();
	const Px_16s *psrc = src.getFirstPtr();
	Px_8u *pdst        = dst.getFirstPtr();
	for (int i = 0; i < total_pixels; ++i) {
		*pdst = static_cast<Px_8u>(std::min(std::max(
            (max - min > 0)
    			? static_cast<float>(*psrc - min) / (max - min) * 255
                : static_cast<float>(*psrc)
			, 0.0f), 255.0f));
		++psrc;
		++pdst;
	}
	return dst;
}