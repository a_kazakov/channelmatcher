#include "Image.h"

#include "channels_operations.h"
#include "img_operations.h"

#include "gauss.h"
#include "sobel.h"
#include "canny.h"
#include "median.h"
#include "img_splitter.h"

#include "ImgMatcher.h"

#include "sharpen.h"
#include "grayworld.h"
#include "autocontrast.h"

#include "channel_matcher.h"

template<typename ValueT>
void check_number(string val_name, ValueT val, ValueT from,
				  ValueT to=numeric_limits<ValueT>::max())
{
	if (val < from)
		throw val_name + string(" is too small");
	if (val > to)
		throw val_name + string(" is too big");
}

template<typename ValueType>
ValueType read_value(string s)
{
	stringstream ss(s);
	ValueType res;
	ss >> res;
	if (ss.fail() || !ss.eof())
		throw string("bad argument: ") + s;
	return res;
}

namespace Main
{

	Image<Px_8u3c> sobel_x(const Image<Px_8u3c> &src_image)
	{
        Image<Px_8u> c1, c2, c3;
        splitChannels(c1, c2, c3, src_image);
        return mixChannels(
            to8bit(sobel_x(to16bit(c1)), 0, 255),
            to8bit(sobel_x(to16bit(c2)), 0, 255),
            to8bit(sobel_x(to16bit(c3)), 0, 255)
        );
        return mixChannels(c1, c2, c3);
    }

	Image<Px_8u3c> sobel_y(const Image<Px_8u3c> &src_image)
	{
        Image<Px_8u> c1, c2, c3;
        splitChannels(c1, c2, c3, src_image);
        return mixChannels(
            to8bit(sobel_y(to16bit(c1)), 0, 255),
            to8bit(sobel_y(to16bit(c2)), 0, 255),
            to8bit(sobel_y(to16bit(c3)), 0, 255)
        );
		return mixChannels(c1, c2, c3);
	}

	Image<Px_8u3c> unsharp(const Image<Px_8u3c> &src_image)
	{
		auto tmp = src_image;
		sharpen(tmp);
		return tmp;
	}

	Image<Px_8u3c> gray_world(const Image<Px_8u3c> &src_image)
	{
		auto tmp = src_image;
		grayWorld(tmp);
		return tmp;
	}

	Image<Px_8u3c> resize(const Image<Px_8u3c> &src_image, double scale = 1.0)
	{
		Image<Px_8u> c1, c2, c3;
		splitChannels(c1, c2, c3, src_image);
		c1 = resizeImage(c1, static_cast<int>(c1.getHeight() * scale),
						 static_cast<int>(c1.getWidth() * scale));
		c2 = resizeImage(c2, static_cast<int>(c2.getHeight() * scale),
						 static_cast<int>(c2.getWidth() * scale));
		c3 = resizeImage(c3, static_cast<int>(c3.getHeight() * scale),
						 static_cast<int>(c3.getWidth() * scale));
		return mixChannels(c1, c2, c3);
	}

	void strExplode(std::vector<std::string> &out, char sym, const std::string &str)
	{
		out.clear();
		size_t last_pos = 0;
		for (size_t i = 0; i < str.length(); ++i) {
			if (str[i] == sym) {
				out.push_back(str.substr(last_pos, i - last_pos));
				last_pos = i + 1;
			}
		}
		if (last_pos != str.size()) {
			out.push_back(str.substr(last_pos, str.size() - last_pos));
		}
	}

	Image<Px_8u3c> custom(const Image<Px_8u3c> &src_image, const std::string &kernel = "1")
	{
		std::vector<std::string> split_l1;
		std::vector<std::vector<std::string>> split_l2;
		strExplode(split_l1, ';', kernel);
		split_l2.resize(split_l1.size());
		int h = static_cast<int>(split_l1.size());
		int w = INT_MAX;
		for (size_t i = 0; i < split_l1.size(); ++i) {
			strExplode(split_l2[i], ',', split_l1[i]);
			w = std::min(w, static_cast<int>(split_l2[i].size()));
		}
		Image<int> ikernel(h, w);
		for (int i = 0; i < h; ++i) {
            if (static_cast<int>(split_l2[i].size()) != w) {
                throw std::string("Incorrect input matrix.");
            }
            for (int j = 0; j < w; ++j) {
				ikernel(i, j) = static_cast<int>(std::round(10000 * read_value<double>(split_l2[i][j])));
			}
		}
        if (h % 2 == 0 || w % 2 == 0) {
            throw std::string("Both convultion kernel dimensions should be odd.");
        }
		Image<Px_8u> c1, c2, c3;
		splitChannels(c1, c2, c3, src_image);
		int rad = std::max(h, w) / 2;
		c1.checkBorders(rad), c1.fillBorders();
		c2.checkBorders(rad), c2.fillBorders();
		c3.checkBorders(rad), c3.fillBorders();
		c1 = to8bit(applyConvultion(to16bit(c1), ikernel, 10000));
		c2 = to8bit(applyConvultion(to16bit(c2), ikernel, 10000));
		c3 = to8bit(applyConvultion(to16bit(c3), ikernel, 10000));
		return mixChannels(c1, c2, c3);
	}

	Image<Px_8u3c> autocontrast(const Image<Px_8u3c> &src_image, double fraction = 0.0)
	{
		check_number("fraction", fraction, 0.0, 0.4);
		auto tmp = src_image;
		autoContrast(tmp, static_cast<float>(fraction));
		return tmp;
	}

	Image<Px_8u3c> median(const Image<Px_8u3c> &src_image, int radius = 0)
	{
		Image<Px_8u> c1, c2, c3;
		splitChannels(c1, c2, c3, src_image);
		return mixChannels(
			median(c1, radius, MEDIAN_SIMPLE),
			median(c2, radius, MEDIAN_SIMPLE),
			median(c3, radius, MEDIAN_SIMPLE)
			);
	}

	Image<Px_8u3c> median_linear(const Image<Px_8u3c> &src_image, int radius = 0)
	{
		Image<Px_8u> c1, c2, c3;
		splitChannels(c1, c2, c3, src_image);
		return mixChannels(
			median(c1, radius, MEDIAN_LINEAR),
			median(c2, radius, MEDIAN_LINEAR),
			median(c3, radius, MEDIAN_LINEAR)
			);
	}

	Image<Px_8u3c> median_const(const Image<Px_8u3c> &src_image, int radius = 0)
	{
		Image<Px_8u> c1, c2, c3;
		splitChannels(c1, c2, c3, src_image);
		return mixChannels(
			median(c1, radius, MEDIAN_CONST),
			median(c2, radius, MEDIAN_CONST),
			median(c3, radius, MEDIAN_CONST)
			);
	}

	Image<Px_8u3c> gaussian(const Image<Px_8u3c> &src_image, double sigma = 1.0, int radius = -1)
	{
		if (radius == -1) {
			radius = static_cast<int>(std::ceil(3 * sigma));
		}
		check_number("sigma", sigma, 0.1, 100.0);
		Image<Px_8u> c1, c2, c3;
		splitChannels(c1, c2, c3, src_image);
		return mixChannels(
			to8bit(slowGauss(to16bit(c1), static_cast<float>(sigma), radius)),
			to8bit(slowGauss(to16bit(c2), static_cast<float>(sigma), radius)),
			to8bit(slowGauss(to16bit(c3), static_cast<float>(sigma), radius))
			);
	}

	Image<Px_8u3c> gaussian_separable(const Image<Px_8u3c> &src_image, double sigma = 1.0, int radius = -1)
	{
		if (radius == -1) {
			radius = static_cast<int>(std::ceil(3 * sigma));
		}
		check_number("sigma", sigma, 0.1, 100.0);
		Image<Px_8u> c1, c2, c3;
		splitChannels(c1, c2, c3, src_image);
		return mixChannels(
			to8bit(gauss(to16bit(c1), static_cast<float>(sigma), radius)),
			to8bit(gauss(to16bit(c2), static_cast<float>(sigma), radius)),
			to8bit(gauss(to16bit(c3), static_cast<float>(sigma), radius))
			);
	}

	Image<Px_8u3c> canny(const Image<Px_8u3c> &src_image, int threshold1 = 20, int threshold2 = 40)
	{
		check_number("threshold1", threshold1, 0, 360);
		check_number("threshold2", threshold2, 0, 360);
		if (threshold1 >= threshold2)
			throw std::string("threshold1 must be less than threshold2");
		Image<Px_8u> tmp = canny(to16bit(toGrayscale(src_image)), threshold1, threshold2);
		return mixChannels(tmp, tmp, tmp);
	}

	Image<Px_8u3c> align(const Image<Px_8u3c> &src_image,
						 const std::string &postprocessing = "", int sp = 1, double fraction = 0.0)
	{
		auto dst_image = matchChannels(toGrayscale(src_image), sp);
		if (postprocessing == "--gray-world") {
			grayWorld(dst_image);
		} else if (postprocessing == "--autocontrast") {
			autoContrast(dst_image, static_cast<float>(fraction));
		} else if (postprocessing == "--unsharp") {
			sharpen(dst_image);
		}
		return dst_image;
	}

}
