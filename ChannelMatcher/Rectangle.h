#pragma once

#include <algorithm>

struct Rectangle
{
	Rectangle(int y, int x, int h, int w);
	Rectangle &operator *= (const Rectangle &r);
	int y, x, h, w;
};

