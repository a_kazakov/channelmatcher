#pragma once

#include <climits>

#include <iostream>
#include <fstream>

#include <vector>
#include <utility>
#include <algorithm>

#include "Image.h"

#include "constants.h"

#include "channels_operations.h"

#include "canny.h"

void unstackChannels(Image<Px_8u> &out1, Image<Px_8u> &out2, Image<Px_8u> &out3, const Image<Px_8u> &src);
