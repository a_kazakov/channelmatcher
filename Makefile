CXX = g++
CXXFLAGS = -Ofast -Wall -march=native -mfpmath=sse -funroll-all-loops -ffast-math -std=gnu++11 -msse3 -pthread
SOURCES = ChannelMatcher/ArgParser.cpp ChannelMatcher/Image.spec.cpp ChannelMatcher/ImgMatcher.cpp ChannelMatcher/Rectangle.cpp ChannelMatcher/autocontrast.cpp ChannelMatcher/canny.cpp ChannelMatcher/channel_matcher.cpp ChannelMatcher/channel_operations.cpp ChannelMatcher/convultions.cpp ChannelMatcher/gauss.cpp ChannelMatcher/grayworld.cpp ChannelMatcher/img_operations.cpp ChannelMatcher/img_splitter.cpp ChannelMatcher/main.cpp ChannelMatcher/median.cpp ChannelMatcher/sharpen.cpp ChannelMatcher/sobel.cpp ChannelMatcher/EasyBMP/EasyBMP.cpp
OBJECTS = $(SOURCES:.cpp=.o)
EXECUTABLE = align
LDFLAGS = -pthread

all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS)
	$(CXX) $(LDFLAGS) $(OBJECTS) -o $@
	
%.o: %.cpp
	$(CXX) $(CXXFLAGS) $< -c -o $@
	
clean:
	rm $(EXECUTABLE) $(OBJECTS)

